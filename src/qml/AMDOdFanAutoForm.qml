// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright 2023 Juan Palacios <jpalaciosdev@gmail.com>

import QtQuick 2.15
import CoreCtrl.UIComponents 1.0

AMD_OD_FAN_AUTO {
  objectName: "AMD_OD_FAN_AUTO"
}
