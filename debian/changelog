corectrl (1.4.3+ds-2) unstable; urgency=medium

  [ Kirill Rekhov ]
  * d/copyright: just change http to https link
  * d/upstream/metadata: update incorrect links to correct ones
  * d/control: update description

 -- Matthias Geiger <werdahias@debian.org>  Sat, 22 Feb 2025 18:54:41 +0100

corectrl (1.4.3+ds-1) unstable; urgency=medium

  * New upstream release
  * Drop tar as maintainer per their request, add myself
  * Rename d/README.debian to d/README.Debian

 -- Matthias Geiger <werdahias@debian.org>  Tue, 07 Jan 2025 19:48:55 +0100

corectrl (1.4.2+ds-2) unstable; urgency=medium

  * d/control: Make corectrl linux-any. Since it requires specific kernel API
    calls building it on HURD is pointless

 -- Matthias Geiger <werdahias@debian.org>  Thu, 07 Nov 2024 14:52:23 +0100

corectrl (1.4.2+ds-1) unstable; urgency=medium

  * New upstream release
  * Enable salsa CI
  * Updated my mail address in d/control

 -- Matthias Geiger <werdahias@debian.org>  Mon, 23 Sep 2024 16:38:33 +0200

corectrl (1.4.1+ds-2) unstable; urgency=medium

  * Change tars' name in d/control, d/copyright

 -- Matthias Geiger <werdahias@debian.org>  Mon, 22 Jul 2024 16:11:50 +0200

corectrl (1.4.1+ds-1) unstable; urgency=medium

  * New upstream release
  * Drop armel-build-failure patch; applied in upstream release

 -- Matthias Geiger <werdahias@riseup.net>  Sat, 01 Jun 2024 14:31:12 +0200

corectrl (1.4.0+ds-2) unstable; urgency=medium

  * Cherry-pick upstream commit to fix build failure on armel (Closes: #1068026) 
  * Bump Standards-Version to 4.7.0; no changes needed
  * Sort d/copyright for better readability

 -- Matthias Geiger <werdahias@riseup.net>  Fri, 17 May 2024 07:50:10 +0200

corectrl (1.4.0+ds-1) unstable; urgency=medium

  * New upstream release
  * Updated Files-Excluded: in d/copyright for new upstream release
  * Updated watchfile for GitLab tag sorting change
  * Updated build-dependencies and versioning in d/control according to
    upstream
  * Drop dependency on libfmt
  * Enable build-time tests

 -- Matthias Geiger <werdahias@riseup.net>  Fri, 29 Mar 2024 11:53:20 +0100

corectrl (1.3.11+ds-1) unstable; urgency=medium

  * New upstream release

 -- Matthias Geiger <werdahias@riseup.net>  Wed, 13 Mar 2024 19:54:21 +0100

corectrl (1.3.10+ds-1) unstable; urgency=medium

  * New upstream release

 -- Matthias Geiger <werdahias@riseup.net>  Mon, 12 Feb 2024 23:36:24 +0100

corectrl (1.3.9+ds-1) unstable; urgency=medium

  * New upstream release

 -- Matthias Geiger <werdahias@riseup.net>  Wed, 17 Jan 2024 22:25:24 +0100

corectrl (1.3.8+ds-1) unstable; urgency=medium

  * New upstream release
  * Added d/gbp.conf
  * Refresh cross.patch
  * Updated copyright information for appdata file

 -- Matthias Geiger <werdahias@riseup.net>  Mon, 06 Nov 2023 19:31:20 +0100

corectrl (1.3.6+ds-1) unstable; urgency=medium

  * New upstream release
  * Sorted d/copyright; added myself for debian/* files
  * Include patch to fix cross building (Closes: #1040535)
  * Remove executable bit for d/watch
  * Updated my mail adress in d/control

 -- Matthias Geiger <werdahias@riseup.net>  Sun, 22 Oct 2023 20:30:51 +0200

corectrl (1.3.5+ds-1) unstable; urgency=medium

  * New upstream release
  * Added missing runtime deps in d/control (Closes: #1034272)
  * Updated extended description

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Sun, 09 Apr 2023 09:27:45 +0200

corectrl (1.3.3+ds-1) unstable; urgency=medium

  * Initial release. (Closes: #986925)

 -- Gürkan Myczko <tar@debian.org>  Mon, 13 Mar 2023 07:20:56 +0100
